/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Write your model definitions here
 */

namespace org.scm

enum ParticipantType {
  o FARMER
  o WAREHOUSE
  o MARKET
  o RETAILER
}

enum QuantityType {
  o ITEM
  o POUNDS
}


/**
 * A shipment being tracked as an asset on the ledger
 */
asset Package identified by packageId {
  o Long packageId
  o String type
  o Long quantity
  o QuantityType quantityType
  o Long Humidity
  o Long Temperature
  o String ReceivedDate
  o String DispatchDate
  --> SupplychainParticipant issuer
  --> SupplychainParticipant owner

}

/**
 * An abstract participant type in this business network
 */
participant SupplychainParticipant identified by ID {
  o String ID
  o String name
  o String address
  o ParticipantType participantType
}

/**
 * A notification that a shipment has been received by the
 * importer and that funds should be transferred from the importer
 * to the grower to pay for the shipment.
 */
transaction MoveProduct {
  --> Package packageId
  --> SupplychainParticipant newOwner
  --> SupplychainParticipant Owner

}